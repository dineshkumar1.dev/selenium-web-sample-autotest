package org.example;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestButton {

    @Test
    public void testButton() {
        System.setProperty("webdriver.chrome.driver", "C:\\selenium-web-sample\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();

        driver.get("C:\\selenium-web-sample-autotese\\src\\test\\resources\\testBtn.html");

        WebElement btn = driver.findElement(By.id("btn1"));
        btn.click();
        String nameBtn = driver.findElement(By.id("btn1")).getAttribute("value");
        System.out.println(nameBtn);
        Assert.assertEquals(nameBtn, "Two");

        //driver.close();


    }
}
